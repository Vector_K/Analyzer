﻿namespace Analyzer.Automate
{
    // Переходы между состояниями автомата
    internal class Transition
    {
        public required int? PrevState { get; init; } // Предыдущее состояние (null - начальное состояние)
        public required char Input { get; init; } // Символ поступающий на вход автомата
        public required int NextState { get; init; } // Следующее состояние
        public required List<dynamic>? Token { get; init; } // Токен (если следующее состояние - конечное)
    }

    public class DFA(int startState, List<int> endStates)
    {
        private int startState = startState; // Начальное состояние
        private List<int> endStates = endStates; // Конечные состояния
        private Dictionary<int, List<Transition>> transitions = new(); // Переходы (текущее состояние, возможные переходы)

        static private int prevState = 0; // Запоминание предыдущего состояния

        /// <summary>
        /// Добавление перехода
        /// </summary>
        /// <param name="state">Текущее состояние</param>
        /// <param name="input">Входной символ</param>
        /// <param name="nextState">Следующее состояние</param>
        /// <param name="newWord">Является ли символ началом нового слова</param>
        /// <param name="token">Токен</param>
        public void AddTransition(int state, char input, int nextState, bool newWord, List<dynamic>? token)
        {
            Transition transition = new Transition()
            {
                PrevState = (newWord)
                    ? null
                    : prevState,
                Input = input,
                NextState = nextState,
                Token = token
            };

            // Если из состояния уже существуют переходы
            if (transitions.TryGetValue(state, out var trans))
            {
                // Если новый переход не существовал
                if (!trans.Exists(tr => tr.PrevState == transition.PrevState &&
                                        tr.Input == transition.Input &&
                                        tr.NextState == transition.NextState &&
                                        tr.Token == transition.Token))
                    trans.Add(transition);
            }
            else
                transitions.Add(state, [transition]);

            prevState = state;
        }

        /// <summary>
        /// Проверка слова на допустимость
        /// </summary>
        /// <param name="input">Слово для проверки</param>
        /// <returns>Токен</returns>
        public List<dynamic>? Check(string input)
        {
            List<int> currentSates = new() { startState }; // Возможные текущие состояния
            List<int> nextStates = new(); // Возможные следующие состояния
            List<int?> prevStates = new() { null }; // Возможные предыдущие состояния
            List<Transition> outTransition = new(); // Возможные переходы

            foreach (char c in input)
            {
                foreach (int state in currentSates)
                {
                    // Если существуют переходы из текущего состояния
                    if (transitions.ContainsKey(state))
                    {
                        foreach (Transition transition in transitions[state])
                        {
                            foreach (var prevSt in prevStates)
                            {
                                // Если состояние совпдает с входными данными
                                if (transition.Input == c && transition.PrevState == prevSt)
                                {
                                    nextStates.Add(transition.NextState);
                                    outTransition.Add(transition);
                                }
                            }
                        }
                    }
                }
                prevStates = [..currentSates];

                currentSates = nextStates;
                nextStates = new();
            }

            foreach (int state in currentSates)
            {
                // Если состояние является конечным
                if (endStates.Contains(state))
                {
                    foreach (var tr in outTransition)
                    {
                        // Вернуть токен
                        if (tr.NextState == state)
                            return tr.Token;
                    }
                }
            }

            return null;
        }
    }
}
