﻿using Analyzer.Automate;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Analyzer
{
    internal class Program
    {
        private static int tokenID = 0; // ID для переменных и значений
        private static List<List<dynamic>> resultTokens = new List<List<dynamic>>(); // Полученные токены
        private static List<int> tokensIDs = new List<int>(); // ID токенов, для определения их порядка следования

        static void Main()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            DFA? dfa = InitDFA("$(SolutionDir)\\..\\..\\..\\..\\Tokens.json");

            stopwatch.Stop();

            if (dfa == null)
            {
                Console.WriteLine("Ошибка при инициализации конечного автомата!");
                return;
            }
            else
                Console.WriteLine($"Конечный автомат был успешно инициализирован. Время инициализации: {stopwatch.ElapsedMilliseconds} мс\n");

            string code = File.ReadAllText("$(SolutionDir)\\..\\..\\..\\..\\code.txt");
            string[] words = code.Split();

            CheckForTokens(words, dfa);

            foreach (var id in tokensIDs)
                Console.WriteLine($"[{resultTokens[id][0]}, {resultTokens[id][1]}]");
        }

        private static int funcLevel = -1; // Уровень вложенности

        /// <summary>
        /// Проверка на токены
        /// </summary>
        /// <param name="words">Массив слов, полученных из кода</param>
        /// <param name="dfa">Конечный автомат</param>
        private static void CheckForTokens(string[] words, DFA dfa)
        {
            string pattern = @"(=|:=|\+|-|:|,|;|<|>|\/|!|\*|\[|\]|\(|\)|\{|}|&|\|)"; // Нужен для отделения специальных символов в отдельные подстроки

            foreach (var word in words)
            {
                if (word == "")
                    continue;

                string[]? substr = null;

                // Разделение слова на подстроки (для таких случаев когда скобки или специльные символы не отделены от переменной или константы пробелом)
                if (word.Length > 1 && word != ":=")
                    substr = Regex.Split(word, pattern);

                if (substr?.Length > 1)
                    CheckForTokens(substr, dfa); // Проверка подстрок
                else
                {
                    // Если переменная уже существует и она удовлетворяет уровню вложенности
                    var indexForID = resultTokens.FindIndex(innerList => innerList.Contains(word));
                    if (indexForID != -1 && (resultTokens[indexForID][2] == funcLevel || resultTokens[indexForID][2] < funcLevel))
                    {
                        tokensIDs.Add(indexForID);
                        continue;
                    }

                    // Проверка на токены в конечном автомате
                    List<dynamic>? token = dfa.Check(word);

                    if (token != null) // Если вернулся токен
                    {
                        // Проверка на уровень вложенности
                        if (token[0] == "PROCEDURE" && token[1] == 0)
                            funcLevel++;
                        if (token[0] == "BLOCK" && token[1] == 1)
                            funcLevel--;

                        // Если токен уже существовал
                        var indexForToken = resultTokens.FindIndex(innerList => innerList.Contains(token[0]) && innerList.Contains(token[1]));
                        if (indexForToken != -1)
                        {
                            tokensIDs.Add(indexForToken);
                            continue;
                        }

                        resultTokens.Add(token);
                    }
                    else
                    {
                        // Добавление токена для переменной
                        if (resultTokens[tokensIDs[tokensIDs.Count - 1]][0] == "PROCEDURE" && resultTokens[tokensIDs[tokensIDs.Count - 1]][1] == 0)
                            resultTokens.Add(new List<dynamic> { "ID", tokenID++, funcLevel - 1, word }); // Чтобы имя процедуры было в уровне видимости ниже процедуры
                        else
                            resultTokens.Add(new List<dynamic> { "ID", tokenID++, funcLevel, word });
                    }

                    tokensIDs.Add(resultTokens.Count - 1);
                }
            }
        }

        /// <summary>
        /// Инициализация конечного автомата
        /// </summary>
        /// <param name="path">Путь к json файлу, содержащему токены</param>
        /// <returns>Конечный автомат</returns>
        private static DFA? InitDFA(string path)
        {
            // Чтение констант из json
            var json = JObject.Parse(File.ReadAllText(path));
            Dictionary<string, List<string>>? tokens = json.ToObject<Dictionary<string, List<string>>>();

            if (tokens != null)
            {
                // Расчет конечных состояний и инициализация автомата
                List<int> endStates = new List<int>();
                var endStatesCount = 1;
                foreach (var token in tokens)
                {
                    for (var i = endStatesCount; i < token.Value.Count + endStatesCount; i++)
                        endStates.Add(i);

                    endStatesCount = endStates.Count;
                }
                DFA dfa = new DFA(0, endStates);

                // Словарь для добавленных состояний (символ, состояние автомата)
                Dictionary<char, int> symbols = new();
                var state = 0;
                var nextState = endStatesCount;
                var endIndex = 0;
                var newWord = true;

                // Генерация автомата
                foreach (var token in tokens)
                {
                    var id = 0;

                    foreach (var value in token.Value)
                    {
                        for (int i = 0; i < value.Length; i++)
                        {
                            // Добавление перехода в конечное состояние
                            if (i + 1 == value.Length)
                            {
                                dfa.AddTransition(state, value[i], endStates[endIndex], newWord, new List<dynamic> { token.Key, id });
                                id++;
                                state = 0;
                                endIndex++;
                                newWord = true;
                                continue;
                            }

                            // Добавление перехода между состояниями
                            // Если состояние уже было добавлено
                            if (symbols.TryGetValue(value[i], out int symbol))
                            {
                                dfa.AddTransition(state, value[i], symbol, newWord, null);
                                state = symbol;
                                newWord = false;
                            }
                            else
                            {
                                symbols.Add(value[i], nextState);

                                dfa.AddTransition(state, value[i], nextState, newWord, null);
                                state = nextState;
                                nextState++;
                                newWord = false;
                            }
                        }
                    }

                    // Запоминаем номер для переменных и значений
                    if (token.Key == "ID")
                        tokenID = id;
                }

                return dfa;
            }

            return null;
        }
    }
}
